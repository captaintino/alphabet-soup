
class wordSearch:

	# Coordinate changes: Right, Diaganol, Down, etc.
	supported_directions = [(0,1), (1,1), (1,0), (1,-1), (0,-1), (-1,-1), (-1,0), (-1, 1) ]

	def __init__(self, opened_file):
		"""Construct wordSearch object

		:param opened_file:
			opened file containig the word search definition
		:type File

		:returns: A new wordSearch object
		"""
		self.grid = []
		self.words = []
		first_line = opened_file.readline()
		self.rows = int(first_line.split('x')[0])
		self.cols = int(first_line.split('x')[1])
		for row in range(self.rows):
			line = opened_file.readline()
			self.grid.append(line.strip().split(' '))
		for line in opened_file:
			self.words.append(line.strip())

	def solve(self):
		"""Solve word search grid

		:returns: A dictionary containig a mapping of each discovered
		word to a tuple of tuples containing starting and ending coordinates
		Example: 'HELLO': ( (0,0), (4,4) )
		"""
		solutions = {}
		for word in self.words:
			solution = self.solve_for_word(word)
			if solution:
				solutions[word] = solution
		return solutions

	def solve_for_word(self, word):
		"""Solve word search grid

		:param word:
			word to find in the grid
		:type String

		:returns: A tuple of tuples containing starting and ending coordinates
		for the word, or False if the word was not found
		Example: ( (0,0), (4,4) )
		"""
		word_length = len(word)
		# Check each square as a starting point for the word
		for row in range(self.rows):
			for col in range(self.cols):
				# Check if first letter of word matches the square
				if self.grid[row][col] == word[0]:
					# Check for the word going in each direction
					for direction in wordSearch.supported_directions:
						found_word = self.check_word(row, col, direction[0], direction[1], word)
						if found_word:
							end_row = row + (direction[0] * (word_length - 1))
							end_col = col + (direction[1] * (word_length - 1))
							return ((row, col), (end_row, end_col))
		return False

	def check_word(self, start_y, start_x, y_movement, x_movement, word):
		"""Solve word search grid

		:param start_y:
			Starting row where the word's first letter is
		:type Int
		:param start_x:
			Starting column where the word's first letter is
		:type Int
		:param y_movement:
			Movement in the up and down direction to look for the word in
		:type Int
		:param x_movement:
			Movement in the left and right direction to look for the word in
		:type Int
		:param word:
			word to find in the grid
		:type String

		:returns: True or False if the word is found
		"""
		end_x = start_x + (x_movement * (len(word) - 1))
		end_y = start_y + (y_movement * (len(word) - 1))
		if (end_x < 0 or end_x > (self.cols - 1) or
			end_y < 0 or end_y > (self.rows - 1)):
			return False
		for index in range(1, len(word)):
			if self.grid[start_y + (y_movement * index)][start_x + (x_movement * index)] != word[index]:
				return False
		else:
			return True
