import sys
from alphabet_soup import wordSearch
import argparse

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Alphabet Soup - Process newspaper word searches')
	parser.add_argument('file', metavar='FILE', type=str, nargs=1,
	                    help='file containing the newspaper word search')

	args = parser.parse_args()
	file_name = args.file[0]
	try:
		opened_file = open(file_name)
	except Exception as e:
		print("ERROR: Filename provided is not valid")
		sys.exit(1)

	grid = wordSearch(opened_file)
	solutions = grid.solve()
	for word in solutions:
		solution = solutions[word]
		print('{} {}:{} {}:{}'.format(word, solution[0][0], solution[0][1], solution[1][0], solution[1][1]))