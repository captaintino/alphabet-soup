# Can be run with `pytest -q run_tests.py`

import pytest
from alphabet_soup import wordSearch

def test_test1():
	test_file_1 = 'tests/test1.txt'
	opened_file = open(test_file_1)
	grid = wordSearch(opened_file)
	solutions = grid.solve()
	print(solutions)
	assert solutions['HELLO'] == ((0,0),(4,4))
	assert solutions['GOOD'] == ((4,0),(4,3))
	assert solutions['BYE'] == ((1,3),(1,1))

def test_test2():
	test_file_2 = 'tests/test2.txt'
	opened_file = open(test_file_2)
	grid = wordSearch(opened_file)
	solutions = grid.solve()
	print(solutions)
	assert solutions['ABC'] == ((0,0),(0,2))
	assert solutions['AEI'] == ((0,0),(2,2))

def test_test3():
	test_file_3 = 'tests/test3.txt'
	opened_file = open(test_file_3)
	grid = wordSearch(opened_file)
	solutions = grid.solve()
	print(solutions)
	assert solutions['AFK'] == ((0,0),(2,0))
	assert solutions['AGM'] == ((0,0),(2,2))
	assert solutions['MHC'] == ((2,2),(0,2))
	assert solutions['IC'] == ((1,3),(0,2))
	assert solutions['OJE'] == ((2,4),(0,4))
	assert solutions['HI'] == ((1,2),(1,3))
	assert solutions['HN'] == ((1,2),(2,3))
	assert solutions['HM'] == ((1,2),(2,2))
	assert solutions['HL'] == ((1,2),(2,1))
	assert solutions['HG'] == ((1,2),(1,1))
	assert solutions['HB'] == ((1,2),(0,1))
	assert solutions['HC'] == ((1,2),(0,2))
	assert solutions['HD'] == ((1,2),(0,3))
